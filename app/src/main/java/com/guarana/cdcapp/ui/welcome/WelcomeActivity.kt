package com.guarana.cdcapp.ui.welcome

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.guarana.cdcapp.R

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
    }
}
